use std::collections::{HashMap, VecDeque};

type Input = Vec<Vec<Vec3>>;
type Vec3 = [i16; 3];
type Matrix3 = [i16; 9];

const ROTATION_MATRICES: [Matrix3; 24] = [
    [1, 0, 0, 0, 1, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 1, 0, -1, 0],
    [1, 0, 0, 0, -1, 0, 0, 0, -1],
    [1, 0, 0, 0, 0, -1, 0, 1, 0],
    [0, 1, 0, 0, 0, 1, 1, 0, 0],
    [0, 1, 0, 1, 0, 0, 0, 0, -1],
    [0, 1, 0, 0, 0, -1, -1, 0, 0],
    [0, 1, 0, -1, 0, 0, 0, 0, 1],
    [0, 0, 1, 1, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 1, 0, -1, 0, 0],
    [0, 0, 1, -1, 0, 0, 0, -1, 0],
    [0, 0, 1, 0, -1, 0, 1, 0, 0],
    [-1, 0, 0, 0, -1, 0, 0, 0, 1],
    [-1, 0, 0, 0, 0, 1, 0, 1, 0],
    [-1, 0, 0, 0, 1, 0, 0, 0, -1],
    [-1, 0, 0, 0, 0, -1, 0, -1, 0],
    [0, -1, 0, 0, 0, -1, 1, 0, 0],
    [0, -1, 0, 1, 0, 0, 0, 0, 1],
    [0, -1, 0, 0, 0, 1, -1, 0, 0],
    [0, -1, 0, -1, 0, 0, 0, 0, -1],
    [0, 0, -1, -1, 0, 0, 0, 1, 0],
    [0, 0, -1, 0, 1, 0, 1, 0, 0],
    [0, 0, -1, 1, 0, 0, 0, -1, 0],
    [0, 0, -1, 0, -1, 0, -1, 0, 0],
];

fn parse_vec3(line: &str) -> Vec3 {
    let mut parts = line.splitn(3, ",").flat_map(|part| part.parse());
    [
        parts.next().unwrap(),
        parts.next().unwrap(),
        parts.next().unwrap(),
    ]
}

fn read_input(path: &str) -> Input {
    std::fs::read_to_string(path)
        .unwrap()
        .split("\n\n")
        .map(|part| part.lines().skip(1).map(parse_vec3).collect())
        .collect()
}

fn manhattan_distance(a: &Vec3, b: &Vec3) -> i16 {
    (a[0] - b[0]).abs() + (a[1] - b[1]).abs() + (a[2] - b[2]).abs()
}

fn diff_hash(a: &Vec3, b: &Vec3) -> u64 {
    let mut l1 = [
        (a[0] - b[0]).abs(),
        (a[1] - b[1]).abs(),
        (a[2] - b[2]).abs(),
    ];

    l1.sort();

    let mut result = 0;
    for dx in l1 {
        result = (result << 32) + dx as u64
    }

    result
}

fn add(a: &Vec3, b: &Vec3) -> Vec3 {
    [a[0] + b[0], a[1] + b[1], a[2] + b[2]]
}

fn sub(a: &Vec3, b: &Vec3) -> Vec3 {
    [a[0] - b[0], a[1] - b[1], a[2] - b[2]]
}

fn rotate(rotation: &Matrix3, point: &Vec3) -> Vec3 {
    [
        (rotation[0] * point[0]) + (rotation[1] * point[1]) + (rotation[2] * point[2]),
        (rotation[3] * point[0]) + (rotation[4] * point[1]) + (rotation[5] * point[2]),
        (rotation[6] * point[0]) + (rotation[7] * point[1]) + (rotation[8] * point[2]),
    ]
}

struct Scanner {
    position: Vec3,
    beacons: Vec<Vec3>,
    pairs: HashMap<u64, (Vec3, Vec3)>,
}

impl Scanner {
    fn from(beacons: &Vec<Vec3>) -> Self {
        let mut pairs = HashMap::new();
        for (offset, a) in beacons.iter().enumerate() {
            for b in beacons[0..offset].iter() {
                pairs.insert(diff_hash(a, b), (a.clone(), b.clone()));
            }
        }
        Scanner {
            position: [0, 0, 0],
            beacons: beacons.clone(),
            pairs,
        }
    }

    fn has_overlap(&self, other: &Self) -> bool {
        self.pairs
            .keys()
            .filter(|&key| other.pairs.contains_key(key))
            .count()
            >= 66
    }

    fn align(&self, root: &Self) -> Self {
        let mut possiblities: HashMap<Vec3, Vec<Vec3>> = HashMap::new();

        for (key, &(rel1, rel2)) in &self.pairs {
            if let Some((abs1, abs2)) = root.pairs.get(key) {
                possiblities
                    .entry(rel1)
                    .and_modify(|value| value.retain(|&p| p == *abs1 || p == *abs2))
                    .or_insert(vec![abs1.clone(), abs2.clone()]);

                possiblities
                    .entry(rel2)
                    .and_modify(|value| value.retain(|&p| p == *abs1 || p == *abs2))
                    .or_insert(vec![abs1.clone(), abs2.clone()]);
            }
        }

        let mut matches: Vec<(Vec3, Vec3)> = possiblities
            .into_iter()
            .filter_map(|(rel, abs)| {
                if abs.len() == 1 {
                    Some((rel, abs[0]))
                } else {
                    None
                }
            })
            .collect();

        let (a0, b0) = matches.pop().unwrap();

        let (rotation, translation) = ROTATION_MATRICES
            .iter()
            .find_map(|rotation| {
                let translation = sub(&b0, &rotate(rotation, &a0));
                let result = matches
                    .iter()
                    .all(|(a, b)| add(&rotate(rotation, a), &translation) == *b);

                if result {
                    Some((rotation, translation))
                } else {
                    None
                }
            })
            .unwrap();

        let mut scanner = Scanner::from(
            &self
                .beacons
                .iter()
                .map(|beacon| add(&rotate(rotation, beacon), &translation))
                .collect(),
        );

        scanner.position = translation;
        scanner
    }
}

fn find_overlap<'a>(needle: &Scanner, haystack: &'a Vec<Scanner>) -> Option<&'a Scanner> {
    for section in haystack {
        if section.has_overlap(needle) {
            return Some(section);
        }
    }
    None
}

fn align(section: &Scanner, absolute_sections: &Vec<Scanner>) -> Option<Scanner> {
    find_overlap(section, absolute_sections).map(|root| section.align(root))
}

fn solve(input: &Input) -> Vec<Scanner> {
    let mut relative: VecDeque<Scanner> = input.iter().map(Scanner::from).collect();
    let mut absolute: Vec<Scanner> = vec![relative.pop_front().unwrap()];

    while let Some(rel_section) = relative.pop_front() {
        match align(&rel_section, &absolute) {
            Some(abs) => {
                absolute.push(abs);
            }
            None => {
                relative.push_back(rel_section);
            }
        }
    }

    absolute
}

fn part1(input: &Input) -> usize {
    let scanners = solve(input);
    let map: Vec<Vec3> = scanners
        .iter()
        .flat_map(|section| section.beacons.iter().cloned())
        .collect();
    map.len()
}

fn part2(input: &Input) -> i16 {
    let scanners = solve(input);
    let mut result = 0;
    for (index, a) in scanners.iter().enumerate() {
        for b in scanners[0..index].iter() {
            result = result.max(manhattan_distance(&a.position, &b.position));
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(79, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(3621, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
