use std::collections::LinkedList;
use std::fs::File;
use std::io::{self, BufRead};

type Input = Vec<Number>;

type Number = LinkedList<Op>;

#[derive(Clone, Debug)]
pub enum Op {
    Open,
    Close,
    Value(u64),
}

pub fn parse_number(line: &str) -> Number {
    line.chars()
        .filter_map(|c| match c {
            '[' => Some(Op::Open),
            ']' => Some(Op::Close),
            '0'..='9' => Some(Op::Value(c as u64 - 48)),
            _ => None,
        })
        .collect()
}

pub fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .map(|line| parse_number(&line.unwrap()))
        .collect()
}

fn add(mut left: Number, mut right: Number) -> Number {
    let mut result = LinkedList::new();

    result.push_back(Op::Open);
    result.append(&mut left);
    result.append(&mut right);
    result.push_back(Op::Close);

    reduce(result)
}

fn reduce(mut number: Number) -> Number {
    loop {
        if try_explode(&mut number) {
            continue;
        }

        if try_split(&mut number) {
            continue;
        }

        return number;
    }
}

fn try_explode(number: &mut Number) -> bool {
    use Op::*;
    let mut level: usize = 0;

    for (index, op) in number.iter().enumerate() {
        match op {
            Open => {
                if level < 4 {
                    level += 1;
                    continue;
                }

                explode(number, index);
                return true;
            }
            Close => {
                level -= 1;
            }
            Value(_) => {}
        };
    }

    false
}

fn try_split(number: &mut Number) -> bool {
    use Op::*;

    for (index, op) in number.iter().enumerate() {
        match op {
            &Value(value) if value >= 10 => {
                split(number, index);
                return true;
            }
            _ => {}
        }
    }

    false
}

fn as_value(number: Option<Op>) -> u64 {
    match number {
        Some(Op::Value(value)) => value,
        _ => panic!("expected value: {:?}", number),
    }
}

fn explode(number: &mut Number, index: usize) {
    use Op::*;
    let mut tail = number.split_off(index);

    tail.pop_front(); // Remove the Open
    let left = as_value(tail.pop_front());
    let right = as_value(tail.pop_front());
    tail.pop_front(); // Remove the Close

    // explode left
    for left_op in number.iter_mut().rev() {
        if let Value(v) = left_op {
            *v += left;
            break;
        }
    }

    // explode right
    for right_op in tail.iter_mut() {
        if let Value(v) = right_op {
            *v += right;
            break;
        }
    }

    number.push_back(Value(0));
    number.append(&mut tail);
}


fn split(number: &mut Number, index: usize) {
    use Op::*;
    let mut tail = number.split_off(index);
    let value = as_value(tail.pop_front());

    let half = value / 2;
    let rem = value % 2;

    number.push_back(Open);
    number.push_back(Value(half));
    number.push_back(Value(half + rem));
    number.push_back(Close);
    number.append(&mut tail);
}

pub fn magnitude(number: Number) -> u64 {
    use Op::*;
    let mut stack = Vec::new();

    for op in number {
        match op {
            Value(value) => {
                stack.push(value);
            }
            Close => {
                let right = stack.pop().unwrap();
                let left = stack.pop().unwrap();
                let result = 3 * left + 2 * right;
                stack.push(result);
            }
            _ => {}
        }
    }

    stack[0]
}

pub fn part1(input: &Input) -> u64 {
    magnitude(input.clone().into_iter().reduce(add).unwrap())
}

pub fn part2(input: &Input) -> u64 {
    let mut max = 0;
    for i in 0..input.len() {
        for j in 0..input.len() {
            let value = magnitude(add(input[i].clone(), input[j].clone()));
            if value > max {
                max = value;
            }
        }
    }
    max
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_small() {
        let a = parse_number("[[[[4,3],4],4],[7,[[8,4],9]]]");
        let b = parse_number("[1,1]");
        let c = parse_number("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
        println!("{:?}", add(a, b));
        println!("{:?}", c);
        // assert_eq!(c, add(a, b));
    }

    #[test]
    fn test_large() {
        let a = parse_number("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]");
        let b = parse_number("[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]");
        let c = parse_number("[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]");
        println!("{:?}", add(a, b));
        println!("{:?}", c);
        // assert_eq!(c, add(a, b));
    }

    #[test]
    fn test_mag() {
        // [[1,2],[[3,4],5]] becomes 143.
        assert_eq!(143, magnitude(parse_number("[[1,2],[[3,4],5]]")));

        // [[[[0,7],4],[[7,8],[6,0]]],[8,1]] becomes 1384.
        assert_eq!(
            1384,
            magnitude(parse_number("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"))
        );

        // [[[[1,1],[2,2]],[3,3]],[4,4]] becomes 445.
        assert_eq!(
            445,
            magnitude(parse_number("[[[[1,1],[2,2]],[3,3]],[4,4]]"))
        );

        // [[[[3,0],[5,3]],[4,4]],[5,5]] becomes 791.
        assert_eq!(
            791,
            magnitude(parse_number("[[[[3,0],[5,3]],[4,4]],[5,5]]"))
        );

        // [[[[5,0],[7,4]],[5,5]],[6,6]] becomes 1137.
        assert_eq!(
            1137,
            magnitude(parse_number("[[[[5,0],[7,4]],[5,5]],[6,6]]"))
        );

        // [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] becomes 3488.
        assert_eq!(
            3488,
            magnitude(parse_number(
                "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
            ))
        );
    }

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(4140, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(3993, part2(&input));
    }
}
