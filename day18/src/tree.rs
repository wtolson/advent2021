use std::fs::File;
use std::io::{self, BufRead};
use std::str::Chars;

pub type Input = Vec<Number>;

#[derive(Clone, Debug)]
pub enum Number {
    Pair(Box<Number>, Box<Number>),
    Value(u64),
}

fn parse_number(chars: &mut Chars) -> Number {
    let c = chars.next().unwrap();
    match c {
        '[' => {
            let left = parse_number(chars);
            assert_eq!(Some(','), chars.next());
            let right = parse_number(chars);
            assert_eq!(Some(']'), chars.next());
            Number::Pair(Box::new(left), Box::new(right))
        }
        '0'..='9' => Number::Value(c as u64 - 48),
        _ => panic!("unexpeced char: {}", c),
    }
}

pub fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .map(|line| parse_number(&mut line.unwrap().chars()))
        .collect()
}

fn add(left: Number, right: Number) -> Number {
    reduce(Number::Pair(Box::new(left), Box::new(right)))
}

fn reduce(mut number: Number) -> Number {
    loop {
        if let Err((result, _, _)) = try_explode(&number, 4) {
            number = result;
            continue;
        }

        if let Err(result) = try_split(&number) {
            number = result;
            continue;
        }

        return number;
    }
}

fn try_explode(number: &Number, level: u64) -> Result<Number, (Number, u64, u64)> {
    use Number::*;

    if let Pair(left, right) = number {
        if level == 0 {
            return Err((Value(0), as_value(left), as_value(right)));
        }

        if let Err((left_result, left_value, right_value)) = try_explode(left, level - 1) {
            let right_result = explode_right(right, right_value);
            let result = Pair(Box::new(left_result), Box::new(right_result));
            return Err((result, left_value, 0));
        }

        if let Err((right_result, left_value, right_value)) = try_explode(right, level - 1) {
            let left_result = explode_left(left, left_value);
            let result = Pair(Box::new(left_result), Box::new(right_result));
            return Err((result, 0, right_value));
        };
    }

    Ok(number.clone())
}

fn try_split(number: &Number) -> Result<Number, Number> {
    use Number::*;

    match number {
        Value(value) => {
            if *value < 10 {
                Ok(number.clone())
            } else {
                Err(split(*value))
            }
        }
        Pair(left, right) => {
            if let Err(left_result) = try_split(left) {
                return Err(Pair(Box::new(left_result), right.clone()));
            }

            if let Err(right_result) = try_split(right) {
                return Err(Pair(left.clone(), Box::new(right_result)));
            }

            Ok(number.clone())
        }
    }
}

fn as_value(number: &Number) -> u64 {
    match number {
        Number::Value(value) => *value,
        _ => panic!("expected number"),
    }
}

fn explode_right(number: &Number, value: u64) -> Number {
    use Number::*;

    if value == 0 {
        return number.clone();
    }

    match number {
        Value(v) => Value(v + value),
        Pair(left, right) => Pair(Box::new(explode_right(left, value)), right.clone()),
    }
}

fn explode_left(number: &Number, value: u64) -> Number {
    use Number::*;

    if value == 0 {
        return number.clone();
    }

    match number {
        Value(v) => Value(v + value),
        Pair(left, right) => Pair(left.clone(), Box::new(explode_left(right, value))),
    }
}

fn split(value: u64) -> Number {
    use Number::*;
    let half = value / 2;
    let rem = value % 2;
    Pair(Box::new(Value(half)), Box::new(Value(half + rem)))
}

fn magnitude(number: Number) -> u64 {
    use Number::*;
    match number {
        Pair(left, right) => (3 * magnitude(*left)) + (2 * magnitude(*right)),
        Value(value) => value,
    }
}

fn print_number(number: &Number) {
    use Number::*;
    match number {
        Pair(left, right) => {
            eprint!("[");
            print_number(left);
            eprint!(",");
            print_number(right);
            eprint!("]");
        }
        Value(value) => {
            eprint!("{}", *value);
        }
    };
}

pub fn part1(input: &Input) -> u64 {
    magnitude(input.clone().into_iter().reduce(add).unwrap())
}

pub fn part2(input: &Input) -> u64 {
    let mut max = 0;
    for i in 0..input.len() {
        for j in 0..input.len() {
            let value = magnitude(add(input[i].clone(), input[j].clone()));
            if value > max {
                max = value;
            }
        }
    }
    max
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_small() {
        let a = parse_number(&mut "[[[[4,3],4],4],[7,[[8,4],9]]]".chars());
        let b = parse_number(&mut "[1,1]".chars());

        print_number(&add(a, b));
        eprintln!();
    }

    #[test]
    fn test_large() {
        let a = parse_number(&mut "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]".chars());
        let b = parse_number(&mut "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]".chars());

        print_number(&add(a, b));
        eprintln!();
        eprintln!("[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]")
    }

    #[test]
    fn test_mag() {
        // [[1,2],[[3,4],5]] becomes 143.
        assert_eq!(143, magnitude(parse_number(&mut "[[1,2],[[3,4],5]]".chars())));

        // [[[[0,7],4],[[7,8],[6,0]]],[8,1]] becomes 1384.
        assert_eq!(1384, magnitude(parse_number(&mut "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]".chars())));

        // [[[[1,1],[2,2]],[3,3]],[4,4]] becomes 445.
        assert_eq!(445, magnitude(parse_number(&mut "[[[[1,1],[2,2]],[3,3]],[4,4]]".chars())));

        // [[[[3,0],[5,3]],[4,4]],[5,5]] becomes 791.
        assert_eq!(791, magnitude(parse_number(&mut "[[[[3,0],[5,3]],[4,4]],[5,5]]".chars())));

        // [[[[5,0],[7,4]],[5,5]],[6,6]] becomes 1137.
        assert_eq!(1137, magnitude(parse_number(&mut "[[[[5,0],[7,4]],[5,5]],[6,6]]".chars())));

        // [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] becomes 3488.
        assert_eq!(3488, magnitude(parse_number(&mut "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]".chars())));

    }

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(4140, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(3993, part2(&input));
    }
}
