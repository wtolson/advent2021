use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};

type Cache = HashMap<(i32, i32), i64>;

fn read_input(path: &str) -> Result<Vec<i32>, io::Error> {
    let file = File::open(path)?;
    let result = io::BufReader::new(file)
        .lines()
        .next()
        .unwrap()?
        .split(",")
        .map(|s| s.parse().unwrap())
        .collect();
    Ok(result)
}

fn run(cache: &mut Cache, ttl: i32, days: i32) -> i64 {
    match cache.get(&(ttl, days)) {
        Some(&result) => result,
        None => {
            if ttl >= days {
                return 1;
            }

            let left = days - ttl - 1;
            let result = run(cache, 6, left) + run(cache, 8, left);

            cache.insert((ttl, days), result);
            result
        }
    }
}

fn run_all(input: &Vec<i32>, days: i32) -> i64 {
    let mut cache: Cache = HashMap::new();
    input.iter().map(|&ttl| run(&mut cache, ttl, days)).sum()
}

fn part1(input: &Vec<i32>) -> Result<i64, &str> {
    Ok(run_all(input, 80))
}

fn part2(input: &Vec<i32>) -> Result<i64, &str> {
    Ok(run_all(input, 256))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt").unwrap();
        assert_eq!(Ok(5934), part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt").unwrap();
        assert_eq!(Ok(26984457539), part2(&input));
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_input("data/input.txt")?;
    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);
    Ok(())
}
