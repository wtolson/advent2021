use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn parse_line(line: String) -> u32 {
    line.parse().unwrap()
}

fn read_input(path: &Path) -> Vec<u32> {
    let file = File::open(path).unwrap();
    io::BufReader::new(file)
        .lines()
        .flatten()
        .map(parse_line)
        .collect()
}

fn part1(input: &Vec<u32>) -> u32 {
    let mut result = 0;
    for i in 0..input.len() - 1 {
        if input[i] < input[i + 1] {
            result += 1;
        }
    }
    return result;
}

fn part2(input: &Vec<u32>) -> u32 {
    let mut windows = Vec::new();
    for i in 0..input.len() - 2 {
        windows.push(input[i] + input[i + 1] + input[i + 2]);
    }
    part1(&windows)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input(Path::new("data/test.txt"));
        assert_eq!(7, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input(Path::new("data/test.txt"));
        assert_eq!(5, part2(&input));
    }
}

fn main() {
    let input = read_input(Path::new("data/input.txt"));

    let result1 = part1(&input);
    println!("part1: {}", result1);

    let result2 = part2(&input);
    println!("part2: {}", result2);
}
