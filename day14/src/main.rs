use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

struct Input {
    template: Vec<char>,
    rules: HashMap<(char, char), char>,
}

fn parse_rule(line: String) -> ((char, char), char) {
    let mut parts = line.splitn(2, " -> ");
    let mut pair = parts.next().unwrap().chars();
    let mut insert = parts.next().unwrap().chars();
    (
        (pair.next().unwrap(), pair.next().unwrap()),
        insert.next().unwrap(),
    )
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    let mut lines = io::BufReader::new(file).lines();

    let template = lines.next().unwrap().unwrap().chars().collect();

    // Skip the next line
    lines.next();

    let rules = lines.flatten().map(parse_rule).collect();

    Input { template, rules }
}

fn step(
    counts: HashMap<(char, char), i64>,
    rules: &HashMap<(char, char), char>,
) -> HashMap<(char, char), i64> {
    let mut next = HashMap::new();
    for (pair, &count) in counts.iter() {
        if let Some(&c) = rules.get(pair) {
            *next.entry((pair.0, c)).or_insert(0) += count;
            *next.entry((c, pair.1)).or_insert(0) += count;
        }
    }
    next
}

fn solve(input: &Input, steps: usize) -> i64 {
    let mut pair_counts: HashMap<(char, char), i64> = HashMap::new();
    for window in input.template.windows(2) {
        let key = (window[0], window[1]);
        *pair_counts.entry(key).or_insert(0) += 1;
    }

    for _ in 0..steps {
        pair_counts = step(pair_counts, &input.rules);
    }

    let mut char_counts: HashMap<char, i64> = HashMap::new();
    for ((c, _), &count) in pair_counts.iter() {
        *char_counts.entry(*c).or_insert(0) += count;
    }

    let last = input.template.last().unwrap();
    *char_counts.entry(*last).or_insert(0) += 1;

    let &max = char_counts.values().max().unwrap();
    let &min = char_counts.values().min().unwrap();

    max - min
}

fn part1(input: &Input) -> i64 {
    solve(input, 10)
}

fn part2(input: &Input) -> i64 {
    solve(input, 40)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(1588, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(2188189693529, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
