fn read_input(path: &str) -> String {
    std::fs::read_to_string(path).unwrap().trim().into()
}

fn decode_hex(s: &str) -> Vec<u8> {
    s.chars()
        .flat_map(|c| match c {
            '0' => [0, 0, 0, 0],
            '1' => [0, 0, 0, 1],
            '2' => [0, 0, 1, 0],
            '3' => [0, 0, 1, 1],
            '4' => [0, 1, 0, 0],
            '5' => [0, 1, 0, 1],
            '6' => [0, 1, 1, 0],
            '7' => [0, 1, 1, 1],
            '8' => [1, 0, 0, 0],
            '9' => [1, 0, 0, 1],
            'A' => [1, 0, 1, 0],
            'B' => [1, 0, 1, 1],
            'C' => [1, 1, 0, 0],
            'D' => [1, 1, 0, 1],
            'E' => [1, 1, 1, 0],
            'F' => [1, 1, 1, 1],
            _ => panic!("invalid hex"),
        })
        .collect()
}

struct BitReader<'a> {
    data: &'a [u8],
    pos: usize,
}

impl<'a> BitReader<'a> {
    fn len(&self) -> usize {
        self.data.len() - self.pos
    }

    fn next(&mut self) -> u8 {
        let result = self.data[self.pos];
        self.pos += 1;
        result
    }

    fn take(&mut self, size: usize) -> &[u8] {
        let result = &self.data[self.pos..self.pos + size];
        self.pos += size;
        result
    }
}

impl<'a> From<&'a [u8]> for BitReader<'a> {
    fn from(data: &'a [u8]) -> Self {
        BitReader { data, pos: 0 }
    }
}

#[derive(Debug)]
struct Frame {
    version: u8,
    packet: Packet,
}

#[derive(Debug)]
enum Packet {
    Value(u64),
    Operator(TypeId, Vec<Frame>),
}

#[derive(Debug)]
enum TypeId {
    Sum = 0,
    Product = 1,
    Min = 2,
    Max = 3,
    Gt = 5,
    Lt = 6,
    Eq = 7,
}

impl From<u64> for TypeId {
    fn from(id: u64) -> Self {
        match id {
            0 => Self::Sum,
            1 => Self::Product,
            2 => Self::Min,
            3 => Self::Max,
            5 => Self::Gt,
            6 => Self::Lt,
            7 => Self::Eq,
            _ => panic!("invalid type id"),
        }
    }
}

fn parse_number(slice: &[u8]) -> u64 {
    slice.iter().fold(0, |acc, &b| (acc << 1) + b as u64)
}

fn parse_literal_value(data: &mut BitReader) -> u64 {
    let mut acc = 0;
    let mut has_more = true;

    while has_more {
        has_more = data.next() == 1;
        for &b in data.take(4) {
            acc = (acc << 1) + (b as u64);
        }
    }

    acc
}

fn parse_subpackets(data: &mut BitReader) -> Vec<Frame> {
    match data.next() {
        0 => {
            // If the length type ID is 0, then the next 15 bits are a number
            // that represents the total length in bits of the sub-packets
            // contained by this packet.
            let length = parse_number(data.take(15)) as usize;
            parse(&mut data.take(length).into())
        }
        1 => {
            // If the length type ID is 1, then the next 11 bits are a number
            // that represents the number of sub-packets immediately contained
            // by this packet.
            let length = parse_number(data.take(11)) as usize;
            let mut packets = Vec::with_capacity(length);

            for _ in 0..length {
                packets.push(parse_packet(data).unwrap())
            }

            packets
        }
        _ => panic!("invalid length id"),
    }
}

fn parse_packet(data: &mut BitReader) -> Option<Frame> {
    if data.len() < 8 {
        return None;
    }

    let version = parse_number(data.take(3)) as u8;
    let type_id = parse_number(data.take(3));

    if type_id == 4 {
        let packet = Packet::Value(parse_literal_value(data));
        return Some(Frame { version, packet });
    }

    // otherwise we have an operator
    let packets = parse_subpackets(data);
    let packet = Packet::Operator(type_id.into(), packets);
    Some(Frame { version, packet })
}

fn parse(data: &mut BitReader) -> Vec<Frame> {
    let mut result = Vec::new();

    while let Some(packet) = parse_packet(data) {
        result.push(packet);
    }

    result
}

fn sum_versions(frames: &Vec<Frame>) -> u64 {
    frames
        .iter()
        .map(|frame| match &frame.packet {
            Packet::Value(_) => frame.version as u64,
            Packet::Operator(_, subpackets) => frame.version as u64 + sum_versions(subpackets),
        })
        .sum()
}

fn eval(frame: &Frame) -> u64 {
    match &frame.packet {
        Packet::Value(value) => *value,
        Packet::Operator(type_id, subpackets) => {
            let mut values = subpackets.iter().map(eval);
            match type_id {
                TypeId::Sum => values.sum(),
                TypeId::Product => values.product(),
                TypeId::Min => values.min().unwrap(),
                TypeId::Max => values.max().unwrap(),
                TypeId::Gt => {
                    let a = values.next().unwrap();
                    let b = values.next().unwrap();
                    if a > b {
                        1
                    } else {
                        0
                    }
                }
                TypeId::Lt => {
                    let a = values.next().unwrap();
                    let b = values.next().unwrap();
                    if a < b {
                        1
                    } else {
                        0
                    }
                }
                TypeId::Eq => {
                    let a = values.next().unwrap();
                    let b = values.next().unwrap();
                    if a == b {
                        1
                    } else {
                        0
                    }
                }
            }
        }
    }
}

fn part1(input: &str) -> u64 {
    let data = decode_hex(input);
    let packets = parse(&mut data.as_slice().into());
    sum_versions(&packets)
}

fn part2(input: &str) -> u64 {
    let data = decode_hex(input);
    let frame = parse_packet(&mut data.as_slice().into()).unwrap();
    eval(&frame)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parser() {
        let data = decode_hex("D2FE28");
        dbg!(parse(&mut data.as_slice().into()));
    }

    #[test]
    fn test_part1() {
        assert_eq!(16, part1("8A004A801A8002F478"));
        assert_eq!(12, part1("620080001611562C8802118E34"));
        assert_eq!(23, part1("C0015000016115A2E0802F182340"));
        assert_eq!(31, part1("A0016C880162017C3686B18A3D4780"));
    }

    #[test]
    fn test_part2() {
        // C200B40A82 finds the sum of 1 and 2, resulting in the value 3.
        assert_eq!(3, part2("C200B40A82"));

        // 04005AC33890 finds the product of 6 and 9, resulting in the value 54.
        assert_eq!(54, part2("04005AC33890"));

        // 880086C3E88112 finds the minimum of 7, 8, and 9, resulting in the value 7.
        assert_eq!(7, part2("880086C3E88112"));

        // CE00C43D881120 finds the maximum of 7, 8, and 9, resulting in the value 9.
        assert_eq!(9, part2("CE00C43D881120"));

        // D8005AC2A8F0 produces 1, because 5 is less than 15.
        assert_eq!(1, part2("D8005AC2A8F0"));

        // F600BC2D8F produces 0, because 5 is not greater than 15.
        assert_eq!(0, part2("F600BC2D8F"));

        // 9C005AC2F8F0 produces 0, because 5 is not equal to 15.
        assert_eq!(0, part2("9C005AC2F8F0"));

        // 9C0141080250320F1802104A08 produces 1, because 1 + 3 = 2 * 2.
        assert_eq!(1, part2("9C0141080250320F1802104A08"));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(input.as_str()));
    println!("part2: {}", part2(input.as_str()));
}
