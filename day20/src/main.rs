use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

struct Input {
    algorithm: Algorithm,
    map: Map,
}

type Algorithm = Vec<bool>;

#[derive(Clone, Debug)]
struct BBox {
    min_x: i32,
    min_y: i32,
    max_x: i32,
    max_y: i32,
}

impl BBox {
    fn new() -> Self {
        BBox {
            min_x: i32::MAX,
            min_y: i32::MAX,
            max_x: i32::MIN,
            max_y: i32::MIN,
        }
    }

    fn insert(&mut self, x: i32, y: i32) {
        self.min_x = self.min_x.min(x);
        self.min_y = self.min_y.min(y);
        self.max_x = self.max_x.max(x);
        self.max_y = self.max_y.max(y);
    }

    fn contains(&self, x: i32, y: i32) -> bool {
        x >= self.min_x && x <= self.max_x && y >= self.min_y && y <= self.max_y
    }

    fn grow(&self, size: i32) -> Self {
        BBox {
            min_x: self.min_x - size,
            min_y: self.min_y - size,
            max_x: self.max_x + size,
            max_y: self.max_y + size,
        }
    }
}

#[derive(Clone, Debug)]
struct Map {
    data: HashSet<(i32, i32)>,
    bbox: BBox,
    background: bool,
}

impl Map {
    fn new() -> Self {
        Map {
            data: HashSet::new(),
            bbox: BBox::new(),
            background: false,
        }
    }

    fn insert(&mut self, x: i32, y: i32) {
        self.data.insert((x, y));
        self.bbox.insert(x, y);
    }

    fn get(&self, x: i32, y: i32) -> bool {
        if self.bbox.contains(x, y) {
            self.data.contains(&(x, y))
        } else {
            self.background
        }
    }

    fn read(&self, x: i32, y: i32) -> usize {
        let mut index = 0;
        for dy in -1..=1 {
            for dx in -1..=1 {
                index = (index << 1) + self.get(x + dx, y + dy) as usize;
            }
        }
        index
    }

    fn len(&self) -> usize {
        self.data.len()
    }
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    let mut lines = io::BufReader::new(file).lines();

    let algorithm = lines
        .next()
        .unwrap()
        .unwrap()
        .chars()
        .map(|c| c == '#')
        .collect();

    lines.next(); // skip the empty one

    let mut map = Map::new();

    for (y, line) in lines.flatten().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                map.insert(x as i32, y as i32);
            }
        }
    }

    Input { algorithm, map }
}

fn step(algorithm: &Algorithm, map: &Map) -> Map {
    let mut next = Map::new();

    next.bbox = map.bbox.grow(1);
    next.background = if map.background {
        algorithm[511]
    } else {
        algorithm[0]
    };

    for x in next.bbox.min_x..=next.bbox.max_x {
        for y in next.bbox.min_y..=next.bbox.max_y {
            let index = map.read(x, y);
            if algorithm[index] {
                next.data.insert((x, y));
            }
        }
    }

    next
}

fn part1(input: &Input) -> usize {
    let mut map = input.map.clone();
    for _ in 0..2 {
        map = step(&input.algorithm, &map);
    }
    map.len()
}

fn part2(input: &Input) -> usize {
    let mut map = input.map.clone();
    for _ in 0..50 {
        map = step(&input.algorithm, &map);
    }
    map.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(35, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(3351, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
