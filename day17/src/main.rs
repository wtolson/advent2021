use text_io::scan;

struct Input {
    x_lower: i32,
    x_upper: i32,
    y_lower: i32,
    y_upper: i32,
}

fn read_input(path: &str) -> Input {
    let data = std::fs::read_to_string(path).unwrap();

    let x_lower: i32;
    let x_upper: i32;
    let y_lower: i32;
    let y_upper: i32;

    scan!(data.bytes() => "target area: x={}..{}, y={}..{}", x_lower, x_upper, y_lower, y_upper);

    Input {
        x_lower,
        x_upper,
        y_lower,
        y_upper,
    }
}

fn part1(input: &Input) -> i32 {
    let vy = -input.y_lower - 1;
    ((vy * vy) + vy) / 2
}

fn valid(bounds: &Input, initial_vx: i32, initial_vy: i32) -> bool {
    let mut x = 0;
    let mut y = 0;

    let mut vx = initial_vx;
    let mut vy = initial_vy;

    loop {
        x += vx;
        if vx != 0 {
            vx -= 1;
        }

        y += vy;
        vy -= 1;

        if x > bounds.x_upper || y < bounds.y_lower {
            return false;
        }

        if x >= bounds.x_lower && y <= bounds.y_upper {
            return true;
        }
    }
}

fn part2(input: &Input) -> i32 {
    let vy_min = input.y_lower;
    let vy_max = -input.y_lower - 1;

    let mut vx_min = 0;
    let vx_max = input.x_upper;

    let mut x = 0;
    while x < input.x_lower {
        vx_min += 1;
        x += vx_max;
    }

    let mut result = 0;
    for vx in vx_min..=vx_max {
        for vy in vy_min..=vy_max {
            if valid(input, vx, vy) {
                result += 1;
            }
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(45, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(112, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
