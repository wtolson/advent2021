use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Input = (u64, u64);

struct Dice(u64);

impl Dice {
    fn next(&mut self) -> u64 {
        let result = self.0 % 100;
        self.0 += 1;
        result + 1
    }

    fn roll(&mut self) -> u64 {
        self.next() + self.next() + self.next()
    }
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    let mut it = io::BufReader::new(file)
        .lines()
        .map(|line| line.expect("empty line").chars().last().unwrap() as u64 - 48);
    (it.next().unwrap(), it.next().unwrap())
}

fn play(mut pos1: u64, mut pos2: u64) -> (u64, u64) {
    let mut dice = Dice(0);
    let mut score1 = 0;
    let mut score2 = 0;

    loop {
        let roll1 = dice.roll();

        pos1 = (pos1 + roll1) % 10;
        score1 += pos1 + 1;

        // println!("player1: roll={} score={} pos={}", roll1, score1, pos1 + 1);
        if score1 >= 1000 {
            break;
        }

        let roll2 = dice.roll();

        pos2 = (pos2 + roll2) % 10;
        score2 += pos2 + 1;

        // println!("player2: roll={} score={} pos={}", roll2, score2, pos2 + 1);
        if score2 >= 1000 {
            break;
        }
    }

    (score1.min(score2), dice.0)
}

fn part1(input: &Input) -> u64 {
    let (losing_score, die_rolls) = play(input.0 - 1, input.1 - 1);
    losing_score * die_rolls
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct PlayerState {
    pos: u64,
    score: u64,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct GameState {
    turn: usize,
    players: [PlayerState; 2],
}

fn play_quantum(cache: &mut HashMap<GameState, [u64; 2]>, state: GameState) -> [u64; 2] {
    if let Some(&result) = cache.get(&state) {
        return result;
    }

    let mut wins: [u64; 2] = [0; 2];

    for roll1 in 1..=3 {
        for roll2 in 1..=3 {
            for roll3 in 1..=3 {
                let mut next = GameState {
                    turn: 1 - (state.turn as i8).signum() as usize,
                    players: state.players.clone(),
                };

                let player = &mut next.players[state.turn];
                player.pos = (player.pos + roll1 + roll2 + roll3) % 10;
                player.score += player.pos + 1;

                if player.score >= 21 {
                    wins[state.turn] += 1;
                } else {
                    let [w1, w2] = play_quantum(cache, next);
                    wins[0] += w1;
                    wins[1] += w2;
                }
            }
        }
    }

    cache.insert(state, wins);
    wins
}

fn part2(input: &Input) -> u64 {
    let mut cache = HashMap::new();

    let state = GameState {
        turn: 0,
        players: [
            PlayerState {
                pos: input.0 - 1,
                score: 0,
            },
            PlayerState {
                pos: input.1 - 1,
                score: 0,
            },
        ],
    };

    let wins = play_quantum(&mut cache, state);
    *wins.iter().max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(739785, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(444356092776315, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
