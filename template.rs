use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

type Input = Vec<Row>;

struct Row(i32);

impl FromStr for Row {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse()
    }
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .flat_map(|line| line.expect("empty line").parse())
        .collect()
}

fn part1(input: &Input) -> i32 {
    todo!("part 1")
}

fn part2(input: &Input) -> i32 {
    todo!("part 2")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(0, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(0, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
