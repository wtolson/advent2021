use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{self, BufRead};

const DIRECTIONS: [(isize, isize); 4] = [
    (-1, 0), // up
    (1, 0),  // down
    (0, -1), // left
    (0, 1),  // right
];

fn read_input(path: &str) -> Vec<Vec<i32>> {
    let file = File::open(path).unwrap();
    io::BufReader::new(file)
        .lines()
        .flatten()
        .map(|line| line.chars().map(|c| c as i32 - 48).collect())
        .collect()
}

fn neighbors(input: &Vec<Vec<i32>>, x: usize, y: usize) -> Vec<(usize, usize)> {
    let mut result = Vec::new();
    let height = input.len() as isize;
    let width = input[0].len() as isize;

    for (dx, dy) in DIRECTIONS {
        let nx = x as isize + dx;
        let ny = y as isize + dy;

        if nx < 0 || nx >= width {
            continue;
        }

        if ny < 0 || ny >= height {
            continue;
        }

        result.push((nx as usize, ny as usize));
    }

    result
}

fn part1(input: &Vec<Vec<i32>>) -> i32 {
    let mut result = 0;
    for (y, row) in input.iter().enumerate() {
        for (x, &value) in row.iter().enumerate() {
            if neighbors(input, x, y)
                .iter()
                .all(|(nx, ny)| input[*ny][*nx] > value)
            {
                result += value + 1;
            }
        }
    }
    result
}

fn part2(input: &Vec<Vec<i32>>) -> i32 {
    let mut heap: BinaryHeap<i32> = BinaryHeap::new();
    let mut seen: Vec<Vec<bool>> = input
        .iter()
        .map(|row| row.iter().map(|_| false).collect())
        .collect();

    for (y, row) in input.iter().enumerate() {
        for (x, &value) in row.iter().enumerate() {
            if value == 9 {
                continue;
            }

            if seen[y][x] {
                continue;
            }

            seen[y][x] = true;

            let mut size: i32 = 1;
            let mut frontier = vec![(x, y)];

            while let Some((fx, fy)) = frontier.pop() {
                for (nx, ny) in neighbors(input, fx, fy) {
                    if input[ny][nx] == 9 {
                        continue;
                    }

                    if seen[ny][nx] {
                        continue;
                    }

                    seen[ny][nx] = true;
                    size += 1;

                    frontier.push((nx, ny));
                }
            }

            heap.push(size);
        }
    }

    heap.pop().unwrap() * heap.pop().unwrap() * heap.pop().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(15, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(1134, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
