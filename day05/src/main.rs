use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::Iterator;
use std::str::FromStr;

#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

impl FromStr for Point {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.splitn(2, ",");
        Ok(Point {
            x: parts.next().ok_or("missing x")?.parse()?,
            y: parts.next().ok_or("missing y")?.parse()?,
        })
    }
}

#[derive(Debug)]
struct Line(Point, Point);

impl Line {
    fn is_diagonal(&self) -> bool {
        self.0.x != self.1.x && self.0.y != self.1.y
    }
}

impl FromStr for Line {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.splitn(2, " -> ");
        Ok(Line(
            parts.next().ok_or("missing start")?.parse()?,
            parts.next().ok_or("missing end")?.parse()?,
        ))
    }
}

fn range(start: i32, end: i32) -> Box<dyn Iterator<Item = i32>> {
    if start < end {
        Box::new(start..=end)
    } else {
        Box::new((end..=start).rev())
    }
}

impl IntoIterator for &Line {
    type Item = (i32, i32);
    type IntoIter = Box<dyn Iterator<Item = Self::Item>>;

    fn into_iter(self) -> Self::IntoIter {
        if self.0.x == self.1.x {
            let x = self.0.x;
            return Box::new(range(self.0.y, self.1.y).map(move |y| (x, y)));
        }

        if self.0.y == self.1.y {
            let y = self.0.y;
            return Box::new(range(self.0.x, self.1.x).map(move |x| (x, y)));
        }

        let x_range = range(self.0.x, self.1.x);
        let y_range = range(self.0.y, self.1.y);
        Box::new(x_range.zip(y_range))
    }
}

fn read_input(path: &str) -> Result<Vec<Line>, Box<dyn Error>> {
    let file = File::open(path)?;
    io::BufReader::new(file)
        .lines()
        .map(|line| line?.parse())
        .collect()
}

fn count_intersections<'a>(lines: impl Iterator<Item = &'a Line>) -> i32 {
    let mut map = HashMap::new();
    for line in lines {
        for point in line {
            match map.get(&point) {
                None => map.insert(point, 1),
                Some(&value) => map.insert(point, value + 1),
            };
        }
    }
    map.values().filter(|&&v| v > 1).count() as i32
}

fn part1(input: &Vec<Line>) -> Result<i32, Box<dyn std::error::Error>> {
    Ok(count_intersections(
        input.iter().filter(|&line| !line.is_diagonal()),
    ))
}

fn part2(input: &Vec<Line>) -> Result<i32, Box<dyn std::error::Error>> {
    Ok(count_intersections(input.iter()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() -> Result<(), Box<dyn Error>> {
        let input = read_input("data/test.txt")?;
        assert_eq!(5, part1(&input)?);
        Ok(())
    }

    #[test]
    fn test_part2() -> Result<(), Box<dyn Error>> {
        let input = read_input("data/test.txt")?;
        assert_eq!(12, part2(&input)?);
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_input("data/input.txt")?;
    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);
    Ok(())
}
