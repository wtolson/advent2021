use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

enum Direction {
    Forward(i32),
    Down(i32),
    Up(i32),
}

fn parse_line(line: String) -> Direction {
    let mut parts = line.splitn(2, " ");
    let direction = parts.next().unwrap();
    let value = parts.next().unwrap().parse().unwrap();
    match direction {
        "forward" => Direction::Forward(value),
        "down" => Direction::Down(value),
        "up" => Direction::Up(value),
        _ => panic!("uknown direction: {}", direction),
    }
}

fn read_input(path: &Path) -> Vec<Direction> {
    let file = File::open(path).unwrap();
    io::BufReader::new(file)
        .lines()
        .flatten()
        .map(parse_line)
        .collect()
}

fn part1(input: &Vec<Direction>) -> i32 {
    let mut x: i32 = 0;
    let mut y: i32 = 0;

    for step in input {
        match step {
            Direction::Forward(value) => x += value,
            Direction::Down(value) => y += value,
            Direction::Up(value) => y -= value,
        }
    }

    x * y
}

fn part2(input: &Vec<Direction>) -> i32 {
    let mut aim: i32 = 0;
    let mut x: i32 = 0;
    let mut y: i32 = 0;

    for step in input {
        match step {
            Direction::Forward(value) => {
                x += value;
                y += aim * value;
            }
            Direction::Down(value) => aim += value,
            Direction::Up(value) => aim -= value,
        }
    }

    x * y
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input(Path::new("data/test.txt"));
        assert_eq!(150, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input(Path::new("data/test.txt"));
        assert_eq!(900, part2(&input));
    }
}

fn main() {
    let input = read_input(Path::new("data/input.txt"));

    let result1 = part1(&input);
    println!("part1: {}", result1);

    let result2 = part2(&input);
    println!("part2: {}", result2);
}
