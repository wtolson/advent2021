use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{self, BufRead};

const DIRECTIONS: [(isize, isize); 4] = [
    (-1, 0), // up
    (1, 0),  // down
    (0, -1), // left
    (0, 1),  // right
];

type Input = Vec<Vec<i32>>;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    cost: i32,
    position: Point,
}

// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .map(|line| {
            line.expect("bad line")
                .chars()
                .map(|c| c as i32 - 48)
                .collect()
        })
        .collect()
}

fn neighbors(position: &Point, width: usize, height: usize) -> Vec<Point> {
    let mut result = Vec::new();

    for (dx, dy) in DIRECTIONS {
        let nx = position.x as isize + dx;
        let ny = position.y as isize + dy;

        if nx < 0 || nx >= width as isize {
            continue;
        }

        if ny < 0 || ny >= height as isize {
            continue;
        }

        result.push(Point {
            x: nx as usize,
            y: ny as usize,
        });
    }

    result
}

fn shortest_path(input: &Input) -> i32 {
    let width = input[0].len();
    let height = input.len();

    let start = Point { x: 0, y: 0 };
    let end = Point {
        x: width - 1,
        y: height - 1,
    };

    let mut dist = vec![vec![i32::MAX; width]; height];
    dist[start.y][start.x] = 0;

    let mut heap = BinaryHeap::new();
    heap.push(State {
        cost: 0,
        position: start,
    });

    while let Some(State { cost, position }) = heap.pop() {
        if position == end {
            return cost;
        }

        // Important as we may have already found a better way
        if cost > dist[position.y][position.x] {
            continue;
        }

        for neighbor in neighbors(&position, width, height) {
            let next = State {
                cost: cost + input[neighbor.y][neighbor.x],
                position: neighbor,
            };

            // If so, add it to the frontier and continue
            if next.cost < dist[neighbor.y][neighbor.x] {
                heap.push(next);
                // Relaxation, we have now found a better way
                dist[neighbor.y][neighbor.x] = next.cost;
            }
        }
    }

    panic!("end not found");
}

fn part1(input: &Input) -> i32 {
    shortest_path(input)
}

fn gen_grid(input: &Input, size: usize) -> Input {
    let width = input[0].len();
    let height = input.len();

    let mut grid = vec![vec![0; size * width]; size * height];

    for (y, row) in input.iter().enumerate() {
        for (x, &value) in row.iter().enumerate() {
            for tile_y in 0..size {
                for tile_x in 0..size {
                    let grid_y = y + (tile_y * height);
                    let grid_x = x + (tile_x * height);

                    let mut grid_value = value + tile_y as i32 + tile_x as i32;
                    if grid_value > 9 {
                        grid_value -= 9;
                    }

                    grid[grid_y][grid_x] = grid_value;
                }
            }
        }
    }

    grid
}

fn part2(input: &Input) -> i32 {
    let grid = gen_grid(input, 5);
    shortest_path(&grid)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(40, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(315, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
