use std::fs::File;
use std::io::{self, BufRead};

fn read_input(path: &str) -> Vec<i32> {
    let file = File::open(path).unwrap();
    io::BufReader::new(file)
        .lines()
        .next()
        .unwrap()
        .unwrap()
        .split(",")
        .map(|v| v.parse().unwrap())
        .collect()
}

fn total_distance(input: &Vec<i32>, root: i32) -> i32 {
    input.iter().map(|&x| (x - root).abs()).sum()
}

fn part1(input: &Vec<i32>) -> i32 {
    let &start = input.iter().min().unwrap();
    let &end = input.iter().max().unwrap();
    (start..=end)
        .map(|root| total_distance(&input, root))
        .min()
        .unwrap()
}

fn triangle_distance(x: i32) -> i32 {
    (x * (x + 1)) / 2
}

fn total_triangle_distance(input: &Vec<i32>, root: i32) -> i32 {
    input
        .iter()
        .map(|&x| triangle_distance((x - root).abs()))
        .sum()
}

fn part2(input: &Vec<i32>) -> i32 {
    let &start = input.iter().min().unwrap();
    let &end = input.iter().max().unwrap();
    (start..=end)
        .map(|root| total_triangle_distance(&input, root))
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(37, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(168, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
