use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

// 0 => abcefg
// 1 => cf
// 2 => acdeg
// 3 => acdfg
// 4 => bcdf
// 5 => abdfg
// 6 => abdefg
// 7 => acf
// 8 => abcdefg
// 9 => abcdfg

// len(2) => 1      cf
// len(3) => 7      acf
// len(4) => 4      bcdf
// len(5) => 2,3,5  abcdefg
// len(6) => 0,6,9  abcdefg
// len(7) => 8      abcdefg

struct Row {
    digits: Vec<String>,
    output: Vec<String>,
}

fn word_vec(s: &str) -> Vec<String> {
    s.split_whitespace().flat_map(String::from_str).collect()
}

impl FromStr for Row {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(" | ");
        Ok(Self {
            digits: word_vec(parts.next().ok_or("missing digits")?),
            output: word_vec(parts.next().ok_or("missing output")?),
        })
    }
}

fn read_input(path: &str) -> Result<Vec<Row>, Box<dyn Error>> {
    let file = File::open(path)?;
    io::BufReader::new(file)
        .lines()
        .map(|line| line?.parse())
        .collect()
}

fn normalize(s: &String) -> String {
    let mut tmp: Vec<char> = s.chars().collect();
    tmp.sort_unstable();
    String::from_iter(tmp)
}

fn intersect(a: &String, b: &String) -> i32 {
    let mut result = 0;
    for c in a.chars() {
        if b.contains(c) {
            result += 1;
        }
    }
    result
}

fn pop(items: &mut Vec<String>, pattern: &String, overlap: i32) -> String {
    let index = items
        .iter()
        .position(|digit| intersect(pattern, digit) == overlap)
        .unwrap();
    items.remove(index)
}

struct Decoder(HashMap<String, i32>);

impl Decoder {
    // Solve:
    // 1 => len(2);
    // 7 => len(3);
    // 4 => len(4);
    // 8 => len(7);
    // 3 => len(5) && len("3" & "1") == 2
    // 2 => len(5) && len("2" & "4") == 2
    // 5 => len(5) && len("5" & "2") == 3
    // 0 => len(6) && len("0" & "5") == 4
    // 9 => len(6) && len("9" & "1") == 2
    // 6 => len(6) && len("9" & "1") == 1
    fn new(digits: &Vec<String>) -> Self {
        let mut result: [Option<String>; 10] =
            [None, None, None, None, None, None, None, None, None, None];

        let mut fives = Vec::new();
        let mut sixes = Vec::new();

        // Pull out unique digits
        for digit in digits {
            // 1 => len(2);
            // 7 => len(3);
            // 4 => len(4);
            // 8 => len(7);
            match digit.len() {
                2 => {
                    result[1] = Some(normalize(digit));
                }
                3 => {
                    result[7] = Some(normalize(digit));
                }
                4 => {
                    result[4] = Some(normalize(digit));
                }
                5 => {
                    fives.push(normalize(digit));
                }
                6 => {
                    sixes.push(normalize(digit));
                }
                7 => {
                    result[8] = Some(normalize(digit));
                }
                _ => {
                    panic!("invalid digit: {}", digit);
                }
            };
        }

        // 3 => len(5) && len("3" & "1") == 2
        let one = result[1].clone().unwrap();
        result[3] = Some(pop(&mut fives, &one, 2));

        // 2 => len(5) && len("2" & "4") == 2
        let four = result[4].clone().unwrap();
        result[2] = Some(pop(&mut fives, &four, 2));

        // 5 => len(5) && len("5" & "4") == 3
        result[5] = fives.pop();

        // 0 => len(6) && len("0" & "5") == 4
        let five = result[5].clone().unwrap();
        result[0] = Some(pop(&mut sixes, &five, 4));

        // 9 => len(6) && len("9" & "1") == 2
        result[9] = Some(pop(&mut sixes, &one, 2));

        // 6 => len(6) && len("9" & "1") == 1
        result[6] = sixes.pop();

        let map = HashMap::from_iter(
            result
                .iter()
                .enumerate()
                .map(|(value, key)| (key.clone().unwrap(), value as i32)),
        );

        Self(map)
    }

    fn decode(&self, digit: &String) -> i32 {
        let Decoder(map) = self;
        *map.get(&normalize(digit)).unwrap()
    }
}

fn part1(input: &Vec<Row>) -> Result<i32, Box<dyn std::error::Error>> {
    let mut result = 0;

    for row in input {
        for signal in &row.output {
            if let 2 | 3 | 4 | 7 = signal.len() {
                result += 1;
            }
        }
    }

    Ok(result)
}

fn part2(input: &Vec<Row>) -> Result<i32, Box<dyn std::error::Error>> {
    let mut result = 0;

    for row in input {
        let mut output = 0;
        let decoder = Decoder::new(&row.digits);

        for digit in &row.output {
            output = (10 * output) + decoder.decode(digit);
        }

        result += output;
    }

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() -> Result<(), Box<dyn Error>> {
        let input = read_input("data/test.txt")?;
        assert_eq!(26, part1(&input)?);
        Ok(())
    }

    #[test]
    fn test_part2() -> Result<(), Box<dyn Error>> {
        let input = read_input("data/test.txt")?;
        assert_eq!(61229, part2(&input)?);
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_input("data/input.txt")?;
    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);
    Ok(())
}
