use std::fs::File;
use std::io::{self, BufRead};

type Input = Vec<Instruction>;

#[derive(Debug)]
enum Instruction {
    // inp a - Read an input value and write it to variable a.
    Inp(char),
    // add a b - Add the value of a to the value of b, then store the result in
    // variable a.
    Add(char, Operand),
    // mul a b - Multiply the value of a by the value of b, then store the
    // result in variable a.
    Mul(char, Operand),
    // div a b - Divide the value of a by the value of b, truncate the result
    // to an integer, then store the result in variable a. (Here, "truncate"
    // means to round the value toward zero.)
    Div(char, Operand),
    // mod a b - Divide the value of a by the value of b, then store the
    // remainder in variable a. (This is also called the modulo operation.)
    Mod(char, Operand),
    // eql a b - If the value of a and b are equal, then store the value 1 in
    // variable a. Otherwise, store the value 0 in variable a.
    Eql(char, Operand),
}

#[derive(Debug)]
enum Operand {
    Register(char),
    Literal(i64),
}

fn parse_instruction(line: String) -> Instruction {
    use Instruction::*;
    let mut parts = line.split(" ");
    let inst = parts.next().unwrap();
    let reg = parts.next().map(parse_register);
    let val = parts.next().map(parse_value);
    match inst {
        "inp" => Inp(reg.unwrap()),
        "add" => Add(reg.unwrap(), val.unwrap()),
        "mul" => Mul(reg.unwrap(), val.unwrap()),
        "div" => Div(reg.unwrap(), val.unwrap()),
        "mod" => Mod(reg.unwrap(), val.unwrap()),
        "eql" => Eql(reg.unwrap(), val.unwrap()),
        _ => panic!("illegal instruction"),
    }
}

fn parse_value(value: &str) -> Operand {
    if value.chars().all(char::is_alphabetic) {
        Operand::Register(parse_register(value))
    } else {
        Operand::Literal(value.parse().unwrap())
    }
}

fn parse_register(value: &str) -> char {
    value.chars().next().unwrap()
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .flatten()
        .map(parse_instruction)
        .collect()
}

fn solve(input: &Input, key: &mut [i64]) -> i64 {
    use Instruction::*;

    let mut stack = Vec::new();

    for (i, block) in input.chunks(18).enumerate() {
        let a = match &block[5] {
            Add('x', Operand::Literal(value)) => *value,
            inst => panic!("unexpected instruction: {:?}", inst),
        };

        let b = match &block[15] {
            Add('y', Operand::Literal(value)) => *value,
            inst => panic!("unexpected instruction: {:?}", inst),
        };

        if a > 0 {
            stack.push((i, b));
        } else {
            let (j, c) = stack.pop().unwrap();

            key[i] = key[j] + a + c;

            if key[i] > 9 {
                key[j] = key[j] - (key[i] - 9);
                key[i] = 9;
            }

            if key[i] < 1 {
                key[j] = key[j] + (1 - key[i]);
                key[i] = 1;
            }
        }

    }

    let mut result = 0;
    for x in key {
        result = (10 * result) + *x;
    }
    result

}

fn part1(input: &Input) -> i64 {
    solve(input, &mut [9; 14])
}

fn part2(input: &Input) -> i64 {
    solve(input, &mut [1; 14])
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
