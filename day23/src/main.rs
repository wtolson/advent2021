use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap};

// #############
// #...........#
// ###A#B#C#D###
//   #A#B#C#D#
//   #########

const EMPTY: u8 = u8::MAX;
const ENERGIES: [u32; 4] = [1, 10, 100, 1000];
const STOPS: [usize; 7] = [0, 1, 3, 5, 7, 9, 10];
const TRANSITIONS: [usize; 4] = [2, 4, 6, 8];

type Room = Vec<u8>;
type Rooms = [Room; 4];
type Hall = [u8; 11];

fn read_input(path: &str) -> Rooms {
    let data = std::fs::read_to_string(path).unwrap();
    let lines: Vec<&str> = data.lines().collect();
    let line1 = lines[2].as_bytes();
    let line2 = lines[3].as_bytes();

    [
        vec![line1[3] - 65, line2[3] - 65],
        vec![line1[5] - 65, line2[5] - 65],
        vec![line1[7] - 65, line2[7] - 65],
        vec![line1[9] - 65, line2[9] - 65],
    ]
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
struct Map {
    hall: Hall,
    rooms: Rooms,
}

#[derive(Clone, Eq, PartialEq)]
struct State {
    cost: u32,
    map: Map,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn solve(start: Rooms, goal: Rooms) -> u32 {
    // dist[node] = current shortest distance from `start` to `node`
    let mut dist: HashMap<Map, u32> = HashMap::new();
    let mut heap: BinaryHeap<State> = BinaryHeap::new();

    // We're at `start`, with a zero cost
    dist.insert(
        Map {
            hall: [EMPTY; 11],
            rooms: start.clone(),
        },
        0,
    );

    heap.push(State {
        cost: 0,
        map: Map {
            hall: [EMPTY; 11],
            rooms: start,
        },
    });

    // Examine the frontier with lower cost nodes first (min-heap)
    while let Some(State { cost, map }) = heap.pop() {
        // Alternatively we could have continued to find all shortest paths
        if map.rooms == goal {
            return cost;
        }

        // Important as we may have already found a better way
        if cost > *dist.get(&map).unwrap_or(&u32::MAX) {
            continue;
        }

        for (index, room) in map.rooms.iter().enumerate() {
            if room
                .iter()
                .all(|&value| value == index as u8 || value == EMPTY)
            {
                continue;
            }

            let cell = room.iter().position(|v| *v != EMPTY).unwrap();
            let kind = room[cell];
            let energy = ENERGIES[kind as usize];
            let hall_start = TRANSITIONS[index];

            for hall_stop in STOPS {
                let hall_min = hall_start.min(hall_stop);
                let hall_max = hall_start.max(hall_stop);

                if (hall_min..=hall_max).any(|pos| map.hall[pos] != EMPTY) {
                    continue;
                }

                let mut next = State {
                    cost: cost + energy * (hall_max - hall_min + cell + 1) as u32,
                    map: map.clone(),
                };

                next.map.rooms[index][cell] = EMPTY;
                next.map.hall[hall_stop] = kind;

                // If so, add it to the frontier and continue
                if next.cost < *dist.get(&next.map).unwrap_or(&u32::MAX) {
                    // Relaxation, we have now found a better way
                    dist.insert(next.map.clone(), next.cost);
                    heap.push(next);
                }
            }
        }

        for (hall_start, &kind) in map.hall.iter().enumerate() {
            if kind == EMPTY {
                continue;
            }

            if !map.rooms[kind as usize]
                .iter()
                .all(|&value| value == kind || value == EMPTY)
            {
                continue;
            }

            // println!("start={} kind={}", hall_start, get_repr(kind));
            // print_map(&map);

            let hall_stop = TRANSITIONS[kind as usize];
            let hall_min = hall_start.min(hall_stop);
            let hall_max = hall_start.max(hall_stop);

            if !(hall_min..=hall_max).all(|pos| pos == hall_start || map.hall[pos] == EMPTY) {
                continue;
            }

            let energy = ENERGIES[kind as usize];
            let cell = map.rooms[kind as usize]
                .iter()
                .rposition(|v| *v == EMPTY)
                .unwrap();

            let mut next = State {
                cost: cost + energy * (hall_max - hall_min + cell + 1) as u32,
                map: map.clone(),
            };

            next.map.hall[hall_start] = EMPTY;
            next.map.rooms[kind as usize][cell] = kind;

            // If so, add it to the frontier and continue
            if next.cost < *dist.get(&next.map).unwrap_or(&u32::MAX) {
                // Relaxation, we have now found a better way
                dist.insert(next.map.clone(), next.cost);
                heap.push(next);
            }
        }
    }

    panic!("goal not reachable")
}

fn part1(input: &Rooms) -> u32 {
    let goal = [vec![0, 0], vec![1, 1], vec![2, 2], vec![3, 3]];
    solve(input.clone(), goal)
}

fn part2(input: &Rooms) -> u32 {
    let goal = [
        vec![0, 0, 0, 0],
        vec![1, 1, 1, 1],
        vec![2, 2, 2, 2],
        vec![3, 3, 3, 3],
    ];

    // #D#C#B#A#
    // #D#B#A#C#
    let start = [
        vec![input[0][0], 3, 3, input[0][1]],
        vec![input[1][0], 2, 1, input[1][1]],
        vec![input[2][0], 1, 0, input[2][1]],
        vec![input[3][0], 0, 2, input[3][1]],
    ];

    solve(start, goal)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(12521, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(44169, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
