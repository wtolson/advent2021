use std::cell::RefCell;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::rc::Rc;
use std::str::FromStr;

const START: &str = "start";
const END: &str = "end";

type Input = Vec<Edge>;

struct Edge {
    from: String,
    to: String,
}

impl FromStr for Edge {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.splitn(2, "-");
        Ok(Self {
            from: parts.next().expect("missing from").to_string(),
            to: parts.next().expect("missing to").to_string(),
        })
    }
}
fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .flat_map(|line| line.expect("empty line").parse())
        .collect()
}

struct Seen(Rc<RefCell<HashMap<String, i32>>>, Option<String>);

impl Seen {
    fn new() -> Self {
        Seen(Rc::new(RefCell::new(HashMap::new())), None)
    }

    fn clone(&self) -> Self {
        Seen(self.0.clone(), None)
    }

    fn contains(&self, k: &String) -> bool {
        match self.0.borrow().get(k) {
            None => false,
            Some(&v) => v > 0,
        }
    }

    fn insert(&mut self, k: &String) -> Self {
        let mut data = (*self.0).borrow_mut();

        match (*data).get_mut(k) {
            None => {
                (*data).insert(k.clone(), 1);
            }
            Some(v) => {
                *v += 1;
            }
        };

        Seen(self.0.clone(), Some(k.clone()))
    }
}

impl Drop for Seen {
    fn drop(&mut self) {
        if let Seen(_, Some(key)) = self {
            let mut data = (*self.0).borrow_mut();
            (*data).get_mut(key).map(|x| *x -= 1);
        }
    }
}

type Map = HashMap<String, Vec<String>>;

fn is_small(cave: &String) -> bool {
    *cave == cave.to_lowercase()
}

fn insert_edge(map: &mut Map, from: &String, to: &String) {
    match map.get_mut(from) {
        None => {
            map.insert(from.clone(), vec![to.clone()]);
        }
        Some(caves) => {
            caves.push(to.clone());
        }
    }
}

fn build_map(edges: &Input) -> Map {
    let mut map = HashMap::new();

    for edge in edges {
        insert_edge(&mut map, &edge.from, &edge.to);
        insert_edge(&mut map, &edge.to, &edge.from);
    }

    map
}

fn run(map: &Map, pos: &String, seen: &mut Seen, check_seen: bool) -> i32 {
    if *pos == END {
        return 1;
    }

    let mut next_seen = if is_small(pos) {
        seen.insert(pos)
    } else {
        seen.clone()
    };

    return map
        .get(pos)
        .unwrap()
        .iter()
        .map(|cave| {
            if *cave == START {
                return 0;
            }

            let cave_seen = seen.contains(cave);
            if check_seen && cave_seen {
                return 0;
            }

            let next_check = if cave_seen { true } else { check_seen };
            run(map, cave, &mut next_seen, next_check)
        })
        .sum();
}

fn part1(input: &Input) -> i32 {
    let map = build_map(input);
    run(&map, &String::from(START), &mut Seen::new(), true)
}

fn part2(input: &Input) -> i32 {
    let map = build_map(input);
    run(&map, &String::from(START), &mut Seen::new(), false)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input_sm = read_input("data/test-sm.txt");
        assert_eq!(10, part1(&input_sm));

        let input_md = read_input("data/test-md.txt");
        assert_eq!(19, part1(&input_md));

        let input_lg = read_input("data/test-lg.txt");
        assert_eq!(226, part1(&input_lg));
    }

    #[test]
    fn test_part2() {
        let input_sm = read_input("data/test-sm.txt");
        assert_eq!(36, part2(&input_sm));

        let input_md = read_input("data/test-md.txt");
        assert_eq!(103, part2(&input_md));

        let input_lg = read_input("data/test-lg.txt");
        assert_eq!(3509, part2(&input_lg));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
