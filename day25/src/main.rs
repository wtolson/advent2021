use std::fs::File;
use std::io::{self, BufRead};

type Map = Vec<Vec<Cell>>;

#[derive(Clone, Copy, PartialEq)]
enum Cell {
    Empty = 0,
    East,
    South,
}

fn parse_cell(c: char) -> Cell {
    match c {
        '.' => Cell::Empty,
        '>' => Cell::East,
        'v' => Cell::South,
        _ => panic!("invalid cell: {}", c),
    }
}

fn parse_row(line: String) -> Vec<Cell> {
    line.chars().map(parse_cell).collect()
}

fn read_input(path: &str) -> Map {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .flatten()
        .map(parse_row)
        .collect()
}

fn step_east(map: Map) -> (bool, Map) {
    let mut changes = false;
    let mut result = map.clone();

    for (i, row) in map.iter().enumerate() {
        for (j, &cell) in row.iter().enumerate() {
            if cell != Cell::East {
                continue;
            }

            let next_j = (j + 1) % row.len();
            let next = map[i][next_j];

            if next != Cell::Empty {
                continue;
            }

            changes = true;
            result[i][j] = Cell::Empty;
            result[i][next_j] = Cell::East;
        }
    }

    (changes, result)
}

fn step_south(map: Map) -> (bool, Map) {
    let mut changes = false;
    let mut result = map.clone();

    for (i, row) in map.iter().enumerate() {
        for (j, &cell) in row.iter().enumerate() {
            if cell != Cell::South {
                continue;
            }

            let next_i = (i + 1) % map.len();
            let next = map[next_i][j];

            if next != Cell::Empty {
                continue;
            }

            changes = true;
            result[i][j] = Cell::Empty;
            result[next_i][j] = Cell::South;
        }
    }

    (changes, result)
}

fn step(map: Map) -> (bool, Map) {
    let (changes_east, map_east) = step_east(map);
    let (changes_south, map_south) = step_south(map_east);
    (changes_east || changes_south, map_south)
}

fn part1(mut map: Map) -> i32 {
    for i in 1.. {
        let (changes, next) = step(map);
        if !changes {
            return i;
        }
        map = next;
    }
    unreachable!();
}

fn part2() -> &'static str {
    "merry christmas!"
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(58, part1(input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(input));
    println!("part2: {}", part2());
}
