use std::fs::File;
use std::io::{self, BufRead};

fn read_input(path: &str) -> Vec<String> {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file).lines().flatten().collect()
}

enum Status {
    Corrupted(i32),
    Incomplete(i64),
}

fn status(line: &String) -> Status {
    let mut stack = Vec::new();
    for c in line.chars() {
        match c {
            '(' => stack.push(')'),
            '[' => stack.push(']'),
            '{' => stack.push('}'),
            '<' => stack.push('>'),
            ')' => {
                if let Some(')') = stack.pop() {
                    continue;
                }
                return Status::Corrupted(3);
            }
            ']' => {
                if let Some(']') = stack.pop() {
                    continue;
                }
                return Status::Corrupted(57);
            }
            '}' => {
                if let Some('}') = stack.pop() {
                    continue;
                }
                return Status::Corrupted(1197);
            }
            '>' => {
                if let Some('>') = stack.pop() {
                    continue;
                }
                return Status::Corrupted(25137);
            }
            _ => panic!("unexpected char: {}", c),
        }
    }

    let mut score = 0;

    for &c in stack.iter().rev() {
        score = (5 * score)
            + match c {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                '>' => 4,
                _ => unreachable!(),
            }
    }

    Status::Incomplete(score)
}

fn part1(input: &Vec<String>) -> i32 {
    input
        .iter()
        .map(|line| {
            if let Status::Corrupted(score) = status(line) {
                score
            } else {
                0
            }
        })
        .sum()
}

fn part2(input: &Vec<String>) -> i64 {
    let mut scores: Vec<i64> = input
        .iter()
        .filter_map(|line| match status(line) {
            Status::Incomplete(score) => Some(score),
            _ => None,
        })
        .collect();

    scores.sort_unstable();
    scores[scores.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(26397, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        assert_eq!(288957, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
