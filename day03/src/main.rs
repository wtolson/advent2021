use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

type Row = Vec<char>;

fn parse_row(line: String) -> Row {
    line.chars().collect()
}

fn read_input(path: &Path) -> Vec<Row> {
    let file = File::open(path).unwrap();
    io::BufReader::new(file)
        .lines()
        .flatten()
        .map(parse_row)
        .collect()
}

fn get_counts(input: &Vec<Row>) -> Vec<i32> {
    let len = input[0].len();
    let mut counts = vec![0; len];

    for row in input {
        for i in 0..len {
            match row[i] {
                '0' => counts[i] -= 1,
                '1' => counts[i] += 1,
                _ => panic!("unkown char"),
            }
        }
    }

    counts
}

fn part1(input: &Vec<Row>) -> i32 {
    let counts = get_counts(input);

    let gamma_src: String = counts
        .iter()
        .map(|&value| if value < 0 { '0' } else { '1' })
        .collect();

    let gamma = i32::from_str_radix(gamma_src.as_str(), 2).unwrap();

    let epsilon_src: String = counts
        .iter()
        .map(|&value| if value < 0 { '1' } else { '0' })
        .collect();

    let epsilon = i32::from_str_radix(epsilon_src.as_str(), 2).unwrap();

    gamma * epsilon
}

fn select(mut candidates: Vec<Row>, mcc: char, lcc: char) -> Row {
    let len = candidates[0].len();
    for i in 0..len {
        let mut count = 0;

        for row in &candidates {
            match row[i] {
                '0' => count -= 1,
                '1' => count += 1,
                _ => panic!("unkown char"),
            }
        }

        let needed = if count < 0 { lcc } else { mcc };
        candidates.retain(|row| row[i] == needed);

        if candidates.len() == 1 {
            return candidates[0].clone();
        }
    }

    panic!("result not found")
}

fn part2(input: &Vec<Row>) -> i32 {
    let o2_src = String::from_iter(select(input.clone(), '1', '0'));
    let o2 = i32::from_str_radix(o2_src.as_str(), 2).unwrap();

    let co2_src = String::from_iter(select(input.clone(), '0', '1'));
    let co2 = i32::from_str_radix(co2_src.as_str(), 2).unwrap();

    o2 * co2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input(Path::new("data/test.txt"));
        assert_eq!(198, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input(Path::new("data/test.txt"));
        assert_eq!(230, part2(&input));
    }
}

fn main() {
    let input = read_input(Path::new("data/input.txt"));
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
