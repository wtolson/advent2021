use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::repeat;
use std::str::FromStr;

#[derive(PartialEq, Eq, Hash, Clone)]
struct Dot {
    x: usize,
    y: usize,
}

impl FromStr for Dot {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.splitn(2, ",");
        Ok(Dot {
            x: parts.next().ok_or("missing x")?.parse()?,
            y: parts.next().ok_or("missing y")?.parse()?,
        })
    }
}
enum Fold {
    Horizontal(usize),
    Vertical(usize),
}

impl FromStr for Fold {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.splitn(2, "=");
        let direction = parts
            .next()
            .ok_or("missing direction")?
            .chars()
            .last()
            .ok_or("bad direction")?;
        let value = parts.next().ok_or("missing value")?.parse()?;

        match direction {
            'x' => Ok(Fold::Vertical(value)),
            'y' => Ok(Fold::Horizontal(value)),
            _ => Err("invalid direction")?,
        }
    }
}

struct Input {
    dots: Vec<Dot>,
    folds: Vec<Fold>,
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    let lines: Vec<String> = io::BufReader::new(file).lines().flatten().collect();

    let split_at = lines
        .iter()
        .position(|line| line.len() == 0)
        .expect("missing blank line");

    let dots = lines[..split_at]
        .iter()
        .flat_map(|line| line.parse())
        .collect();

    let folds = lines[split_at + 1..]
        .iter()
        .flat_map(|line| line.parse())
        .collect();

    Input { dots, folds }
}

fn fold_dots(dots: &HashSet<Dot>, fold: &Fold) -> HashSet<Dot> {
    dots.iter()
        .map(|dot| match fold {
            &Fold::Vertical(col) => {
                if dot.x > col {
                    Dot {
                        x: (2 * col) - dot.x,
                        y: dot.y,
                    }
                } else {
                    Dot { x: dot.x, y: dot.y }
                }
            }
            &Fold::Horizontal(row) => {
                if dot.y > row {
                    Dot {
                        x: dot.x,
                        y: (2 * row) - dot.y,
                    }
                } else {
                    Dot { x: dot.x, y: dot.y }
                }
            }
        })
        .collect()
}

fn draw(dots: &HashSet<Dot>) -> String {
    let height = dots.iter().map(|dot| dot.y).max().unwrap() + 1;
    let width = dots.iter().map(|dot| dot.x).max().unwrap() + 1;

    let mut grid: Vec<Vec<char>> = repeat(repeat(' ').take(width).collect())
        .take(height)
        .collect();

    for dot in dots {
        grid[dot.y][dot.x] = '#';
    }

    let rows: Vec<String> = grid.iter().map(|row| row.iter().collect()).collect();
    rows.join("\n")
}

fn part1(input: &Input) -> usize {
    let dots: HashSet<Dot> = input.dots.iter().cloned().collect();
    fold_dots(&dots, &input.folds[0]).len()
}

fn part2(input: &Input) -> String {
    let init: HashSet<Dot> = input.dots.iter().cloned().collect();

    let dots = input
        .folds
        .iter()
        .fold(init, |acc, fold| fold_dots(&acc, fold));

    draw(&dots)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt");
        assert_eq!(17, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt");
        let output = String::from(
            "#####\n\
             #   #\n\
             #   #\n\
             #   #\n\
             #####",
        );
        assert_eq!(output, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2:\n\n{}\n", part2(&input));
}
