use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};

const BOARD_WIDTH: usize = 5;
const BOARD_HIGHT: usize = 5;
const BOARD_SIZE: usize = BOARD_WIDTH * BOARD_HIGHT;

#[derive(Debug, Clone, Copy)]
struct Board<T> {
    data: [T; BOARD_SIZE],
}

impl Board<()> {
    fn pos(x: usize, y: usize) -> usize {
        (x * BOARD_WIDTH) + y
    }
}

impl<T> Board<T> {
    fn new(default: T) -> Board<T>
    where
        T: Copy,
    {
        Board {
            data: [default; BOARD_SIZE],
        }
    }

    fn set(&mut self, x: usize, y: usize, value: T) {
        self.data[Board::pos(x, y)] = value;
    }

    fn get(&self, x: usize, y: usize) -> &T {
        &self.data[Board::pos(x, y)]
    }
}

#[derive(Debug)]
struct Input {
    numbers: Vec<i32>,
    boards: Vec<Board<i32>>,
}

fn parse_numbers(lines: &mut impl Iterator<Item = String>) -> Vec<i32> {
    let line = lines.next().unwrap(); // take the first line
    line.split(',').flat_map(|part| part.parse()).collect()
}

fn parse_board(lines: &mut impl Iterator<Item = String>) -> Option<Board<i32>> {
    // We should start with a blank line
    match lines.next() {
        None => return None,
        Some(_) => (),
    }

    let mut board = Board::new(0);

    for x in 0..BOARD_HIGHT {
        match lines.next() {
            None => return None,
            Some(line) => {
                let mut row = line.split_whitespace();
                for y in 0..BOARD_WIDTH {
                    board.set(x, y, row.next().unwrap().parse().unwrap());
                }
            }
        }
    }

    Some(board)
}

fn parse_boards(lines: &mut impl Iterator<Item = String>) -> Vec<Board<i32>> {
    let mut boards = Vec::new();
    while let Some(board) = parse_board(lines) {
        boards.push(board);
    }
    boards
}

fn read_input(path: &str) -> Result<Input, io::Error> {
    let file = File::open(path)?;
    let mut lines = io::BufReader::new(file).lines().flatten();

    let numbers = parse_numbers(&mut lines);
    let boards = parse_boards(&mut lines);

    Ok(Input { numbers, boards })
}
struct PlayableBoard {
    board: Board<i32>,
    state: Board<bool>,
    lookup: HashMap<i32, usize>,
}

impl PlayableBoard {
    fn new(board: Board<i32>) -> PlayableBoard {
        let mut lookup = HashMap::with_capacity(BOARD_SIZE);
        for (index, &value) in board.data.iter().enumerate() {
            lookup.insert(value, index);
        }
        PlayableBoard {
            board,
            lookup,
            state: Board::new(false),
        }
    }

    fn clone(&self) -> PlayableBoard {
        PlayableBoard {
            board: self.board.clone(),
            lookup: self.lookup.clone(),
            state: self.state.clone(),
        }
    }

    fn mark(&mut self, value: i32) -> bool {
        match self.lookup.get(&value) {
            None => false,
            Some(&index) => {
                self.state.data[index] = true;
                true
            }
        }
    }

    fn check_row(&self, x: usize) -> bool {
        self.state.data[x * BOARD_WIDTH..(x + 1) * BOARD_WIDTH]
            .iter()
            .all(|&s| s)
    }

    fn check_col(&self, y: usize) -> bool {
        for x in 0..BOARD_HIGHT {
            if !self.state.get(x, y) {
                return false;
            }
        }
        true
    }

    fn check_diag_down(&self) -> bool {
        for x in 0..BOARD_HIGHT {
            if !self.state.get(x, x) {
                return false;
            }
        }
        true
    }

    fn check_diag_up(&self) -> bool {
        for x in 0..BOARD_HIGHT {
            if !self.state.get(x, BOARD_HIGHT - x) {
                return false;
            }
        }
        true
    }
    fn check_winner(&self) -> bool {
        if (0..BOARD_WIDTH).any(|x| self.check_row(x)) {
            return true;
        }

        if (0..BOARD_HIGHT).any(|y| self.check_col(y)) {
            return true;
        }

        if self.check_diag_down() {
            return true;
        }

        if self.check_diag_up() {
            return true;
        }

        false
    }

    fn sum_unmarked(&self) -> i32 {
        let mut result = 0;

        for i in 0..BOARD_SIZE {
            if self.state.data[i] {
                continue;
            }
            result += self.board.data[i];
        }

        result
    }
}

fn find_winner(input: &Input) -> Result<(i32, PlayableBoard), &str> {
    let mut boards: Vec<PlayableBoard> = input
        .boards
        .iter()
        .map(|&b| PlayableBoard::new(b))
        .collect();

    for &value in &input.numbers {
        for board in boards.iter_mut() {
            if board.mark(value) {
                if board.check_winner() {
                    return Ok((value, board.clone()));
                }
            }
        }
    }

    Err("no winner found")
}

fn find_loser(input: &Input) -> Result<(i32, PlayableBoard), &str> {
    let mut boards: Vec<RefCell<PlayableBoard>> = input
        .boards
        .iter()
        .map(|&b| RefCell::new(PlayableBoard::new(b)))
        .collect();

    for &value in &input.numbers {
        if boards.len() == 1 {
            let mut board = boards[0].borrow_mut();
            if board.mark(value) {
                if board.check_winner() {
                    return Ok((value, board.clone()));
                }
            }
        } else {
            boards.retain(|cell| {
                let mut board = cell.borrow_mut();
                if !board.mark(value) {
                    return true;
                }
                !board.check_winner()
            });
        }
    }

    Err("no winner found")
}

fn part1(input: &Input) -> Result<i32, &str> {
    let (value, winner) = find_winner(input)?;
    Ok(value * winner.sum_unmarked())
}

fn part2(input: &Input) -> Result<i32, &str> {
    let (value, loser) = find_loser(input)?;
    Ok(value * loser.sum_unmarked())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test.txt").unwrap();
        assert_eq!(Ok(4512), part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test.txt").unwrap();
        let (value, loser) = find_loser(&input).unwrap();

        assert_eq!(148, loser.sum_unmarked());
        assert_eq!(13, value);

        assert_eq!(Ok(1924), part2(&input));
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_input("data/input.txt")?;
    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);
    Ok(())
}
