use std::fs::File;
use std::io::{self, BufRead};

type Input = Vec<Step>;

#[derive(Clone, Debug)]
struct Step {
    state: bool,
    cuboid: Cuboid,
}

#[derive(Clone, Debug)]
struct Cuboid {
    x: Range,
    y: Range,
    z: Range,
}

impl Cuboid {
    fn size(&self) -> i64 {
        self.x.size() * self.y.size() * self.z.size()
    }

    fn contains(&self, other: &Self) -> bool {
        self.x.contains(&other.x) && self.y.contains(&other.y) && self.z.contains(&other.z)
    }

    fn intersection(&self, other: &Self) -> Option<Self> {
        Some(Cuboid {
            x: self.x.intersection(&other.x)?,
            y: self.y.intersection(&other.y)?,
            z: self.z.intersection(&other.z)?,
        })
    }
}

#[derive(Clone, Debug)]
struct Range(i64, i64);

impl Range {
    fn size(&self) -> i64 {
        self.1 - self.0 + 1
    }

    fn contains(&self, other: &Self) -> bool {
        self.0 <= other.0 && self.1 >= other.1
    }

    fn intersection(&self, other: &Self) -> Option<Self> {
        if other.1 < self.0 || other.0 > self.1 {
            None
        } else {
            Some(Range(self.0.max(other.0), self.1.min(other.1)))
        }
    }
}

fn parse_range(s: &str) -> Range {
    let mut parts = s[2..].split("..");
    Range(
        parts.next().unwrap().parse().unwrap(),
        parts.next().unwrap().parse().unwrap(),
    )
}

fn parse_step(s: &str) -> Step {
    let mut parts = s.split(" ");
    let state = parts.next().unwrap() == "on";

    let mut cuboid = parts.next().unwrap().split(",");
    let x = parse_range(cuboid.next().unwrap());
    let y = parse_range(cuboid.next().unwrap());
    let z = parse_range(cuboid.next().unwrap());

    Step {
        state,
        cuboid: Cuboid { x, y, z },
    }
}

fn read_input(path: &str) -> Input {
    let file = File::open(path).expect("failed not found");
    io::BufReader::new(file)
        .lines()
        .map(|line| parse_step(line.unwrap().as_str()))
        .collect()
}

fn count(cuboid: &Cuboid, substeps: &[Step]) -> i64 {
    let mut result = cuboid.size();

    for (index, step) in substeps.iter().enumerate() {
        if let Some(intersection) = cuboid.intersection(&step.cuboid) {
            result -= count(&intersection, &substeps[index + 1..])
        }
    }

    result
}

fn solve(steps: &Vec<Step>) -> i64 {
    let mut result = 0;
    for (index, step) in steps.iter().enumerate() {
        if !step.state {
            continue;
        }
        result += count(&step.cuboid, &steps[index + 1..]);
    }
    result
}

fn part1(input: &Input) -> i64 {
    let bounds = Cuboid {
        x: Range(-50, 50),
        y: Range(-50, 50),
        z: Range(-50, 50),
    };

    let steps: Vec<Step> = input
        .iter()
        .filter(|Step { cuboid, .. }| bounds.contains(cuboid))
        .cloned()
        .collect();

    solve(&steps)
}

fn part2(input: &Input) -> i64 {
    solve(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = read_input("data/test-part1.txt");
        assert_eq!(590784, part1(&input));
    }

    #[test]
    fn test_part2() {
        let input = read_input("data/test-part2.txt");
        assert_eq!(2758514936282235, part2(&input));
    }
}

fn main() {
    let input = read_input("data/input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
