use std::fs::File;
use std::io::{self, BufRead};
use std::thread::sleep;
use std::time::Duration;

const SIZE: usize = 10;

type Grid = [[i32; SIZE]; SIZE];

const DIRECTIONS: [(isize, isize); 8] = [
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (0, 1),
    (1, 1),
    (1, 0),
    (1, -1),
    (0, -1),
];

fn read_input(path: &str) -> Grid {
    let file = File::open(path).expect("failed not found");
    let mut grid = [[0; SIZE]; SIZE];

    for (y, line) in io::BufReader::new(file).lines().enumerate() {
        for (x, c) in line.expect("error reading line").chars().enumerate() {
            grid[y][x] = c.to_string().parse().expect("invalid int");
        }
    }

    grid
}

fn push_neighbors(frontier: &mut Vec<(usize, usize)>, x: usize, y: usize) {
    for (dx, dy) in DIRECTIONS {
        let nx = x as isize + dx;
        if nx < 0 || nx >= SIZE as isize {
            continue;
        }

        let ny = y as isize + dy;
        if ny < 0 || ny >= SIZE as isize {
            continue;
        }

        frontier.push((nx as usize, ny as usize));
    }
}

fn step(grid: &mut Grid) -> i32 {
    let mut flashes = 0;
    let mut frontier: Vec<(usize, usize)> = Vec::new();

    for y in 0..SIZE {
        for x in 0..SIZE {
            frontier.push((x, y));
        }
    }

    while let Some((x, y)) = frontier.pop() {
        grid[y][x] += 1;

        if grid[y][x] == 10 {
            flashes += 1;
            push_neighbors(&mut frontier, x, y);
        }
    }

    for y in 0..SIZE {
        for x in 0..SIZE {
            if grid[y][x] > 9 {
                grid[y][x] = 0
            }
        }
    }

    flashes
}

fn part1(input: &mut Grid) -> i32 {
    let mut flashes = 0;
    for _ in 0..100 {
        flashes += step(input);
    }
    flashes
}

fn part2(input: &mut Grid) -> i32 {
    let mut steps = 1;
    while step(input) != 100 {
        steps += 1;
    }
    steps
}

fn draw(grid: &Grid, step: i32) {
    print!("{}{}", termion::cursor::Goto(1, 4), termion::clear::CurrentLine);
    println!("step: {}\n", step);
    for y in 0..SIZE {
        for x in 0..SIZE {
            match grid[y][x] {
                0 => print!(
                    "{}{}0{}",
                    termion::style::Bold,
                    termion::color::Red.fg_str(),
                    termion::style::Reset
                ),
                v => print!("{}", v),
            }
        }
        print!("\n");
    }
}

fn visualization(grid: &mut Grid) {
    for i in 0..10000 {
        draw(grid, i);
        sleep(Duration::from_secs_f32(0.1));
        step(grid);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let mut input = read_input("data/test.txt");
        assert_eq!(1656, part1(&mut input));
    }

    #[test]
    fn test_part2() {
        let mut input = read_input("data/test.txt");
        assert_eq!(195, part2(&mut input));
    }
}

fn main() {
    let input = read_input("data/input.txt");

    print!("{}{}", termion::clear::All, termion::cursor::Goto(1, 1));
    println!("part1: {}", part1(&mut input.clone()));
    println!("part2: {}", part2(&mut input.clone()));
    print!("");
    visualization(&mut input.clone());
}
